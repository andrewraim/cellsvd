/* --------------------------------------------------------------  */
/* (C)Copyright 2001,2007,                                         */
/* International Business Machines Corporation,                    */
/* Sony Computer Entertainment, Incorporated,                      */
/* Toshiba Corporation,                                            */
/*                                                                 */
/* All Rights Reserved.                                            */
/* --------------------------------------------------------------  */
/* PROLOG END TAG zYx                                              */
#include "../common.h"

#include <libspe2.h>
#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/wait.h>
#include <string.h>
#include <math.h>
#include <stdbool.h>

#include <sys/time.h>


#define sign(n) ((n) < 0.0 ? -1.0 : ((n) > 0.0 ? 1.0 : 0.0) )
#define min(a,b) ((a) < (b) ? a : b)

struct timezone tzone;
struct timeval time0, time1;
int sec, usec;

void print_matrix(struct sparse_float_matrix* matrix)
{
	size_t i, j;

	printf("matrix[%d][%d]:\n", matrix->uiRows, matrix->uiCols);

	for (i = 0; i < matrix->uiRows; i++)
	{
		printf("matrix[%d] has %d nonzero columns\n", i, matrix->rows[i].uiCols);

		for (j = 0; j < matrix->rows[i].uiCols; j++)
		{
			printf("matrix[%d][%d] = %f\n", i, matrix->rows[i].elements[j].uiCol,
				matrix->rows[i].elements[j].data);
		} 
	}
}

struct complex_float jacobi(float a, float b, float g)
{
	struct complex_float x;

	float w = (b - a) / (2.0 * g);

	if (w == 0)
	{
		puts("w equals zero!!!");
	}

	float t = sign(w) / (fabs(w) + sqrt(1.0 + w*w));
	x.real = 1.0 / sqrt(1.0 + t*t);
	x.imag = t * x.real;

	debug2("t = %f / (%f + sqrt(1.0 + %f*%f)) = %f\n", sign(w), fabs(w), w, w, t);

	return x;
}

float sparse_inner_product(struct sparse_float_matrix_row row1, struct sparse_float_matrix_row row2)
{
	size_t idx1 = 0, idx2 = 0;
	bool finished = false;
	float innerprod = 0.0;

	while (!finished)
	{
		if (row1.elements[idx1].uiCol < row2.elements[idx2].uiCol)
		{
			idx1++;
		}
		else if (row1.elements[idx1].uiCol > row2.elements[idx2].uiCol)
		{
			idx2++;
		}
		else
		{
			innerprod += row1.elements[idx1].data * row2.elements[idx2].data;
			idx1++;
			idx2++;
		}

		if (idx1 >= row1.uiCols) finished = true;
		if (idx2 >= row2.uiCols) finished = true;
	}

	return innerprod;
}

bool hestenes_jacobi_calculation(struct sparse_float_matrix* matrix, size_t lb_i, size_t ub_i, 
	size_t lb_j, size_t ub_j, float G_matrix[R][R], struct complex_float P_matrix[R][R], float delta)
{
	debug1("hestenes_jacobi_calculation [%d, %d] x [%d, %d]\n", lb_i, ub_i, lb_j, ub_j);

	bool converged = true;
	bool finished = false;
	size_t i, j;
	float a, b, g, data_i, data_j;

	// Vectors of norms for row block i and row block j
	float theta_i[R];
	float theta_j[R];

	// Structures for doing the global iteration through all rows in blocks i and j
	size_t current_min_column_index;
	size_t state_vector_i[R];
	size_t state_vector_j[R];
	size_t* p_current_state_i;
	size_t* p_current_state_j;

	// TBD: Use memset on each matrix/vector to initialize
	for (i = 0; i < R; i++)
	{
		for (j = 0; j < R; j++)
		{
			G_matrix[i][j] = 0.0;

			P_matrix[i][j].real = 0.0;
			P_matrix[i][j].imag = 0.0;
		}
	}

	for (i = 0; i < R; i++)
	{
		theta_i[i] = 0.0;
		state_vector_i[i] = 0;
	}

	for (j = 0; j < R; j++)
	{
		theta_j[j] = 0.0;
		state_vector_j[j] = 0;
	}

	debug2("Calculation: Finished initializing stuff\n");

	do
	{
		finished = true;
		current_min_column_index = INT_MAX;

		// Find the minimum column index for the first element in each row
		// Keep track of all the rows that contain this minimum column index
		// Accumulate the norms for these rows
		// For each pair of rows from block i and block j
		//   Accumulate the dot product of this pair


		// Step 1: Find the minimum column index of all in-progress rows
		// If any rows are in progress, we're not finished at the end of this iteration
		for (i = lb_i; i <= ub_i; i++)
		{
			p_current_state_i = &(state_vector_i[i % R]);
			if (*p_current_state_i < matrix->rows[i].uiCols)
			{
				current_min_column_index = min(matrix->rows[i].elements[*p_current_state_i].uiCol, current_min_column_index);
				finished = false;
			}
		}

		for (j = lb_j; j <= ub_j; j++)
		{
			p_current_state_j = &(state_vector_j[j % R]);
			if (*p_current_state_j < matrix->rows[j].uiCols)
			{
				current_min_column_index = min(matrix->rows[j].elements[*p_current_state_j].uiCol, current_min_column_index);
				finished = false;
			}
		}

		debug3("Calculation: Minimum next column index across all rows is %d, finished = %d\n", current_min_column_index, finished);

		// Step 2: For all rows pairs i and j whose next element has the same index as current_min_column_index
		// in both rows, accumulate the dot product
		for (i = lb_i; i <= ub_i; i++)
		{
			for (j = lb_j; j <= ub_j; j++)
			{
				if (i >= j)
				{
					continue;
				}

				// Ensure both rows i and j contain the current_min_column_index as their next index
				// and that we haven't finished iterating through them
				// Otherwise skip this iteration

				p_current_state_i = &(state_vector_i[i % R]);
				if (*p_current_state_i >= matrix->rows[i].uiCols 
					|| current_min_column_index != matrix->rows[i].elements[*p_current_state_i].uiCol)
				{
					continue;
				}

				p_current_state_j = &(state_vector_j[j % R]);
				if (*p_current_state_j >= matrix->rows[j].uiCols 
					|| current_min_column_index != matrix->rows[j].elements[*p_current_state_j].uiCol)
				{
					continue;
				}

				data_i = matrix->rows[i].elements[*p_current_state_i].data;
				data_j = matrix->rows[j].elements[*p_current_state_j].data;

				debug3("dot producting matrix(%d, %d) and matrix(%d, %d)\n",
					i, 
					matrix->rows[i].elements[*p_current_state_i].uiCol,
					j, 
					matrix->rows[j].elements[*p_current_state_j].uiCol);

				G_matrix[i % R][j % R] += data_i * data_j;
			}
		}

		debug2("Calculation: Finished step 2\n");

		// Step 3: For all rows whose next element has the same index as current_min_column_index
		// accumulate the norm of the row
		for (i = lb_i; i <= ub_i; i++)
		{
			p_current_state_i = &(state_vector_i[i % R]);
			if (*p_current_state_i < matrix->rows[i].uiCols 
				&& current_min_column_index == matrix->rows[i].elements[*p_current_state_i].uiCol)
			{

				data_i = matrix->rows[i].elements[*p_current_state_i].data;
				theta_i[i % R] += data_i * data_i;
				(*p_current_state_i)++;
			}
		}

		for (j = lb_j; j <= ub_j; j++)
		{
			p_current_state_j = &(state_vector_j[j % R]);
			if (*p_current_state_j < matrix->rows[j].uiCols 
				&& current_min_column_index == matrix->rows[j].elements[*p_current_state_j].uiCol)
			{

				data_j = matrix->rows[j].elements[*p_current_state_j].data;
				theta_j[j % R] += data_j * data_j;
				(*p_current_state_j)++;
			}
		}


		debug2("Calculation: Finished step 3\n");

	} while (!finished);

	debug2("Calculation: Finished calculating theta vectors and P matrix\n");

	// Now the norms and dotproducts should be calculated, so we can calculate the jacobi values
	for (i = lb_i; i <= ub_i; i++)
	{
		for (j = lb_j; j <= ub_j; j++)
		{
			if (i >= j)
			{
				continue;
			}

			a = theta_i[i % R];
			b = theta_j[j % R];
			g = G_matrix[i % R][j % R];

			debug2("a = norm(row_block[i, %d]) = %f\n", i, a);
			debug2("b = norm(row_block[j, %d]) = %f\n", j, b);
			debug2("g = dotprod(row_block[i, %d], row_block[j, %d]) = %f\n", i, j, g);

			if (fabs(g) > delta)
			{
				converged = false;
			}

			if (fabs(g) > EPSILON)
			{
				P_matrix[i % R][j % R] = jacobi(a, b, g);
			}
			else
			{
				P_matrix[i % R][j % R].real = 1.0;
				P_matrix[i % R][j % R].imag = 0.0;
			}

			debug2("P for row pair(%d, %d) = (%f, %f)\n", i, j, 
				P_matrix[i % R][j % R].real, P_matrix[i % R][j % R].imag);
		}
	}

	return converged;
}

void hestenes_jacobi_application(struct sparse_float_matrix* matrix, size_t lb_i, size_t ub_i, 
	size_t lb_j, size_t ub_j, struct complex_float P_matrix[R][R])
{
	debug1("hestenes_jacobi_application [%d, %d] x [%d, %d]\n", lb_i, ub_i, lb_j, ub_j);

	bool finished = false;
	size_t i, j;
	float data_i, data_j;

	// Structures for doing the global iteration through all rows in blocks i and j
	size_t current_min_column_index;
	size_t state_vector_i[R];
	size_t state_vector_j[R];
	size_t* p_current_state_i;
	size_t* p_current_state_j;

	// TBD: Use memset on each matrix/vector to initialize
	for (i = 0; i < R; i++)
	{
		state_vector_i[i] = 0;
		state_vector_j[i] = 0;
	}

	do
	{
		finished = true;
		current_min_column_index = INT_MAX;

		// Find the minimum column index for the first element in each row
		// Keep track of all the rows that contain this minimum column index
		// Apply the hestenes_jacobi rotation on the next column, for each pair of rows
		// that match


		// Step 1: Find the minimum column index of all in-progress rows
		// If any rows are in progress, we're not finished at the end of this iteration
		for (i = lb_i; i <= ub_i; i++)
		{
			p_current_state_i = &(state_vector_i[i % R]);
			if (*p_current_state_i < matrix->rows[i].uiCols)
			{
				current_min_column_index = min(matrix->rows[i].elements[*p_current_state_i].uiCol, current_min_column_index);
				finished = false;
			}
		}

		for (j = lb_j; j <= ub_j; j++)
		{
			p_current_state_j = &(state_vector_j[j % R]);
			if (*p_current_state_j < matrix->rows[j].uiCols)
			{
				current_min_column_index = min(matrix->rows[j].elements[*p_current_state_j].uiCol, current_min_column_index);
				finished = false;
			}
		}

		debug3("Application: Minimum next column index across all rows is %d, finished = %d\n", current_min_column_index, finished);

		// Step 2: For all rows pairs i and j whose next element has the same index as current_min_column_index
		// in both rows, apply the hestenes_jacobi rotation two these elements
		for (i = lb_i; i <= ub_i; i++)
		{
			for (j = lb_j; j <= ub_j; j++)
			{
				if (i >= j)
				{
					continue;
				}

				// Ensure both rows i and j contain the current_min_column_index as their next index
				// and that we haven't finished iterating through them
				// Otherwise skip this iteration

				p_current_state_i = &(state_vector_i[i % R]);
				if (*p_current_state_i >= matrix->rows[i].uiCols 
					|| current_min_column_index != matrix->rows[i].elements[*p_current_state_i].uiCol)
				{
					continue;
				}

				p_current_state_j = &(state_vector_j[j % R]);
				if (*p_current_state_j >= matrix->rows[j].uiCols 
					|| current_min_column_index != matrix->rows[j].elements[*p_current_state_j].uiCol)
				{
					continue;
				}

				data_i = matrix->rows[i].elements[*p_current_state_i].data;
				data_j = matrix->rows[j].elements[*p_current_state_j].data;
				struct complex_float x = P_matrix[i % R][j % R];

				matrix->rows[i].elements[*p_current_state_i].data = x.real * data_i - x.imag * data_j;
				matrix->rows[j].elements[*p_current_state_j].data = x.imag * data_i + x.real * data_j;

				debug2("app rows %d and %d\n", i, j);
				debug2("matrix->rows[%d].elements[%d].data <- %f * %f - %f * %f = %f\n", 
					i, *p_current_state_i, x.real, data_i, x.imag, data_j, 
					matrix->rows[i].elements[*p_current_state_i].data);

				debug2("matrix->rows[%d].elements[%d].data <- %f * %f - %f * %f = %f\n", 
					j, *p_current_state_j, x.imag, data_i, x.real, data_j, 
					matrix->rows[j].elements[*p_current_state_j].data);
				debug2("\n");
			}
		}

		debug2("Application: Finished step 2\n");

		// Step 3: For each row that matched, iterate to the next element
		for (i = lb_i; i <= ub_i; i++)
		{
			p_current_state_i = &(state_vector_i[i % R]);
			if (*p_current_state_i < matrix->rows[i].uiCols 
				&& current_min_column_index == matrix->rows[i].elements[*p_current_state_i].uiCol)
			{

				(*p_current_state_i)++;
			}
		}

		for (j = lb_j; j <= ub_j; j++)
		{
			p_current_state_j = &(state_vector_j[j % R]);
			if (*p_current_state_j < matrix->rows[j].uiCols 
				&& current_min_column_index == matrix->rows[j].elements[*p_current_state_j].uiCol)
			{

				(*p_current_state_j)++;
			}
		}


		debug2("Application: Finished step 3\n");


	} while (!finished);
}

bool process_work_block(struct sparse_float_matrix* matrix, size_t lb_i, size_t ub_i, 
	size_t lb_j, size_t ub_j, float delta)
{
	debug1("Processing work block [%d, %d] x [%d, %d]\n", lb_i, ub_i, lb_j, ub_j);

	bool converged = true;

	// TBD: May want to reuse these rather than create them for each job
	// G is a matrix of dotproducts, P is a matrix of jacobi calculations
	// These are stored between pair of rows from block i and block j
	float G_matrix[R][R];
	struct complex_float P_matrix[R][R];

	// Perform the HJ calculation, then the HJ application
	converged &= hestenes_jacobi_calculation(matrix, lb_i, ub_i, lb_j, ub_j, G_matrix, P_matrix, delta);
	hestenes_jacobi_application(matrix, lb_i, ub_i, lb_j, ub_j, P_matrix);

	return converged;
}

void do_svd(struct sparse_float_matrix* matrix, size_t row_block_size)
{
	size_t i, diagonal, row_idx;
	float sumdotprods = 0.0, dotprod;
	bool converged = false;
	size_t lb_i, ub_i, lb_j, ub_j;
	float sigma[matrix->uiRows];
	size_t iterations = 0;

	for (i = 0; i < matrix->uiRows; i++)
	{
		dotprod = sparse_inner_product(matrix->rows[i], matrix->rows[i]);
		debug1("row[%d] dot row[%d] = %f\n", i, i, dotprod);
		sumdotprods += dotprod;
	}

	float delta = EPSILON * sumdotprods;
	debug1("delta = %f\n", delta);

	size_t num_blocks = (size_t) ceil(matrix->uiRows * 1.0 / row_block_size);

	do
	{
		iterations++;
		debug1("Starting iteration = %d\n", iterations);

		converged = true;
		for (diagonal = 0; diagonal < num_blocks; diagonal++)
		{
			debug1("Processing diagonal %d of the work matrix\n", diagonal);
			for (row_idx = 0; row_idx < num_blocks - diagonal; row_idx++)
			{
				lb_i = row_idx * row_block_size;
				ub_i = (size_t) min(lb_i + row_block_size - 1, matrix->uiRows - 1);
				lb_j = (row_idx + diagonal) * row_block_size;
				ub_j = (size_t) min(lb_j + row_block_size - 1, matrix->uiRows - 1);
				converged = process_work_block(matrix, lb_i, ub_i, lb_j, ub_j, delta);
			}
		}

	} while (converged != true);

	// Now find the sigmas
	// TBD: Should we return this to the caller? Or write it to disk? Should we return anything else?
	for (i = 0; i < matrix->uiRows; i++)
	{
		sigma[i] = sqrt( sparse_inner_product(matrix->rows[i], matrix->rows[i]) );
		printf("sigma[%d] = %f\n", i, sigma[i]); fflush(stdout);
	}

}

int main(void)
{
	size_t M = 10, N = 3;
	size_t i = 0, j;

	// 3.0000    4.0000    1.0000
	// 8.0000    1.0000    9.0000
	// 8.0000    7.0000    7.0000
	// 2.0000    2.0000    2.0000
	// 4.0000    6.0000    2.0000
	// 9.0000    1.0000    6.0000
	// 9.0000    6.0000    9.0000
	// 2.0000    4.0000    8.0000
	// 8.0000    6.0000    3.0000
	// 8.0000    2.0000    5.0000

	struct sparse_float_matrix matrix;
	matrix.uiRows = M;
	matrix.uiCols = N;
	matrix.rows = malloc(M * sizeof(struct sparse_float_matrix_row));

	for (i = 0; i < matrix.uiRows; i++)
	{
		matrix.rows[i].uiCols = matrix.uiCols;
		matrix.rows[i].elements = malloc(matrix.rows[i].uiCols * sizeof(struct sparse_float_matrix_row_element));
		matrix.rows[i].elements[0].uiCol = 0;
		matrix.rows[i].elements[1].uiCol = 1;
		matrix.rows[i].elements[2].uiCol = 2;
	}

	i=0;
	matrix.rows[i].elements[0].data = 3.0;
	matrix.rows[i].elements[1].data = 4.0;
	matrix.rows[i].elements[2].data = 1.0;

	i++;
	matrix.rows[i].elements[0].data = 8.0;
	matrix.rows[i].elements[1].data = 1.0;
	matrix.rows[i].elements[2].data = 9.0;

	i++;
	matrix.rows[i].elements[0].data = 8.0;
	matrix.rows[i].elements[1].data = 7.0;
	matrix.rows[i].elements[2].data = 7.0;

	i++;
	matrix.rows[i].elements[0].data = 2.0;
	matrix.rows[i].elements[1].data = 2.0;
	matrix.rows[i].elements[2].data = 2.0;

	i++;
	matrix.rows[i].elements[0].data = 4.0;
	matrix.rows[i].elements[1].data = 6.0;
	matrix.rows[i].elements[2].data = 2.0;

	i++;
	matrix.rows[i].elements[0].data = 9.0;
	matrix.rows[i].elements[1].data = 1.0;
	matrix.rows[i].elements[2].data = 6.0;

	i++;
	matrix.rows[i].elements[0].data = 9.0;
	matrix.rows[i].elements[1].data = 6.0;
	matrix.rows[i].elements[2].data = 9.0;

	i++;
	matrix.rows[i].elements[0].data = 2.0;
	matrix.rows[i].elements[1].data = 4.0;
	matrix.rows[i].elements[2].data = 8.0;

	i++;
	matrix.rows[i].elements[0].data = 8.0;
	matrix.rows[i].elements[1].data = 6.0;
	matrix.rows[i].elements[2].data = 3.0;

	i++;
	matrix.rows[i].elements[0].data = 8.0;
	matrix.rows[i].elements[1].data = 2.0;
	matrix.rows[i].elements[2].data = 5.0;


	// Initialize a small (but dense) matrix for testing
	/*
	struct sparse_float_matrix matrix;
	matrix.uiRows = M;
	matrix.uiCols = N;
	matrix.rows = malloc(M * sizeof(struct sparse_float_matrix_row));
	for (i = 0; i < matrix.uiRows; i++)
	{
		matrix.rows[i].uiCols = matrix.uiCols;
		matrix.rows[i].elements = malloc(matrix.rows[i].uiCols * sizeof(struct sparse_float_matrix_row_element));
		for (j = 0; j < matrix.rows[i].uiCols; j++)
		{
			matrix.rows[i].elements[j].uiCol = j;
			matrix.rows[i].elements[j].data = i * N * 1.0 + j;
		}
	}
	*/

	print_matrix(&matrix);

	printf("PPE-only SVD, M = %d, N = %d, R = %d\n", M, N, R);  fflush(stdout);

	gettimeofday( &time0, &tzone );


	// Algorithm starts here
	do_svd(&matrix, R);
	// Algorithm ends here

	print_matrix(&matrix);

	gettimeofday( &time1, &tzone );

	sec  = time1.tv_sec  - time0.tv_sec ;
	usec = time1.tv_usec - time0.tv_usec ;
	if ( usec < 0 ) { sec--; usec+=1000000 ; }


	printf("PPE: Done matrix[%d][%d]: time=%d.%d\n",M,N,sec,usec);  fflush(stdout);

	# ifndef NEVER
	{
		int error ;
		error = 0;

		// Check results here

		if (error) { printf("Error in SVD?.\n"); fflush(stdout); }
		else { printf("SVD is correct?\n"); fflush(stdout); }
	}
	# endif

	# ifdef NEVER
		// print results here
	# endif


	return 0;
}
