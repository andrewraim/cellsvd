#ifndef __common_h__
#define __common_h__

/* This union helps clarify calling parameters between the PPE and the SPE. */

typedef union
{
  unsigned long long ull;
  unsigned int ui[2];
  void *p;
} addr64;

struct sparse_float_matrix_row_element
{
  unsigned int uiCol;
  float data;
};

struct sparse_float_matrix_row
{
  unsigned int uiCols;
  struct sparse_float_matrix_row_element * elements;
};

struct sparse_float_matrix
{
  unsigned int uiRows;
  unsigned int uiCols;
  struct sparse_float_matrix_row * rows;
}; 

struct complex_float
{
  float real;
  float imag;
}; 

struct index_pair
{
  unsigned int idx_left;
  unsigned int idx_right;
};


// total 1000*1000 = 4,000,000 B
// total 4000*4000 = 64,000,000 B
# define tsize 64*32
# define loops 1

#define EPSILON 0.0001

#define R 2

#ifdef DEBUG1
#define debug1(...) fprintf(stdout, __VA_ARGS__); fflush(stdout);
#else
#define debug1(...)
#endif

#ifdef DEBUG2
#define debug2(...) fprintf(stdout, __VA_ARGS__); fflush(stdout);
#else
#define debug2(...)
#endif

#ifdef DEBUG3
#define debug3(...) fprintf(stdout, __VA_ARGS__); fflush(stdout);
#else
#define debug3(...)
#endif


#endif /* __common_h__ */
