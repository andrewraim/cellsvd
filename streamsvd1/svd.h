#ifndef SVD_H_
#define SVD_H_

#include <gsl/gsl_linalg.h>
#include <gsl/gsl_matrix_double.h>
#include <gsl/gsl_vector_double.h>

void dosvd(gsl_matrix * A, gsl_vector * S, gsl_matrix_complex * P, gsl_vector * sigma, gsl_matrix * Ut, gsl_matrix * Vt);
double dotproduct(gsl_vector_view A, gsl_vector_view B);
gsl_complex jacobi(double a, double b, double g);
double norm(gsl_vector_view A);
void print_matrix(const char* name, const gsl_matrix* matrix);
void print_vector(const char* name, const gsl_vector* vector);
void get_sorted_vector_order(gsl_vector* index_vect, const gsl_vector* data_vect);

#endif /*SVD_H_*/
